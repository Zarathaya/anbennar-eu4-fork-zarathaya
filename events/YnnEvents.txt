
namespace = ynn_events

# Ynn Urbanization
country_event = {
	id = ynn_events.0
	title = ynn_events.0.t
	desc = ynn_events.0.d
	picture = CITY_VIEW_eventPicture
	
	fire_only_once = yes
	major = yes
	
	trigger = {
		is_year = 1620
        OR = {
            AND = {
                has_dlc = "Mandate of Heaven"
                1139 = { is_prosperous = yes }
                1171 = { is_prosperous = yes }
                1191 = { is_prosperous = yes }
            }
            NOT = {
                has_dlc = "Mandate of Heaven"
				1139 = { devastation = 1 }
                1171 = { devastation = 1 }
                1191 = { devastation = 1 }
            }
        }
		OR = {
			AND = {
				has_dlc = "Rights of Man"
				is_great_power = yes
			}
			AND = {
				NOT = { has_dlc = "Rights of Man" }
				total_development = 300
			}
		}
		capital_scope = { superregion = ynn_superregion }
		has_institution = global_trade
	}

	mean_time_to_happen = {
		months = 1
	}
	
	# Hooray
	option = {
        name = ynn_events.0.a
		add_prestige = 10
		add_stability_or_adm_power = yes
		1139 = {
			remove_province_modifier = ynn_urban_decline
			add_center_of_trade_level = 1
			owner = {
				add_opinion = {
					who = ROOT
					modifier = opinion_grateful
				}
			}
		}
		1171 = {
			remove_province_modifier = ynn_urban_decline
			add_center_of_trade_level = 1
			owner = {
				add_opinion = {
					who = ROOT
					modifier = opinion_grateful
				}
			}
		}
		1191 = {
			remove_province_modifier = ynn_urban_decline
			add_center_of_trade_level = 1
			owner = {
				add_opinion = {
					who = ROOT
					modifier = opinion_grateful
				}
			}
		}
	}
}

#iosahar drifts away
#country_event = {
#	id = ynn_events.6
#	title = ynn_events.6.t
#	desc = ynn_events.6.d
#	picture = ACCUSATION_eventPicture
#	goto = iosahar_into_tributary_capital
#	
#	is_triggered_only = yes
#	trigger = {
#		has_country_modifier = ynn_diplo
#		any_subject_country = {
#			is_subject_of_type = ynnic_iosahar
#			NOT = { is_neighbor_of = ROOT }
#			liberty_desire = 20
#		}
#		NOT = { 
#			has_country_flag = yrw_overlord_debating
#			stability = 3
#		}
#	}
#	
#	immediate = {
#		hidden_effect = {
#			random_subject_country = {
#				limit = {
#					is_subject_of_type = ynnic_iosahar
#					NOT = { is_neighbor_of = ROOT }
#					liberty_desire = 20
#				}
#				save_event_target_as = iosahar_into_tributary_target
#				capital_scope = {
#					save_event_target_as = iosahar_into_tributary_capital
#				}
#			}
#			random_subject_country = {
#				limit = {
#					is_subject_of_type = ynnic_iosahar
#					NOT = { is_neighbor_of = ROOT }
#					liberty_desire = 50	#Prioritizes disloyal vassals
#				}
#				save_event_target_as = iosahar_into_tributary_target
#				capital_scope = {
#					save_event_target_as = iosahar_into_tributary_capital
#				}
#			}
#		}
#	}
#	
#	option = {
#		name = ynn_events.6.a
#		trigger = {
#			has_dlc = "El Dorado"
#			authority = 5
#		}
#		ai_chance = {
#			factor = 5
#			modifier = {
#				factor = 2
#				NOT = { ynnic_iosahar = 3 }
#			}
#		}
#		
#		custom_tooltip = iosahar_goes_tributary_dlc_tooltip
#		add_authority = -5
#	}
#	
#	option = {
#		name = ynn_events.6.a
#		trigger = {
#			NOT = { has_dlc = "El Dorado" }
#			legitimacy = 5		#insert whatever currency you replace legitimacy with
#		}
#		ai_chance = {
#			factor = 5
#			modifier = {
#				factor = 2
#				NOT = { ynnic_iosahar = 3 }
#			}
#		}
#		
#		custom_tooltip = iosahar_goes_tributary_no_dlc_tooltip
#		add_legitimacy = -5		#insert whatever currency you replace legitimacy with
#	}
#
#	option = {
#		name = ynn_events.6.b
#		
#		trigger = {
#			has_dlc = "Mandate of Heaven"
#		}
#		ai_chance = {
#			factor = 3
#			modifier = {
#				factor = 3
#				ynnic_iosahar = 8
#			}
#		}
#		
#		custom_tooltip = iosahar_goes_tributary_dlc_tooltip
#		event_target:iosahar_into_tributary_target = {
#			ROOT = {
#				create_subject = {
#					subject_type = tributary_state
#					subject = PREV
#				}
#			}
#			hidden_effect = { country_event = { id = ynn_events.18 } }
#		}
#	}
#
#	option = {
#		name = ynn_events.6.c
#		
#		trigger = {
#			NOT = { has_dlc = "Mandate of Heaven" }
#		}
#		ai_chance = {
#			factor = 3
#			modifier = {
#				factor = 3
#				ynnic_iosahar = 8
#			}
#		}
#		
#		custom_tooltip = iosahar_goes_tributary_no_dlc_tooltip
#		event_target:iosahar_into_tributary_target = {
#			grant_independence = yes
#			hidden_effect = { country_event = { id = ynn_events.18 } }
#		}
#	}
#}

#The Great Iosahar Insurrection - For the debate-triggered version, see ynn_debate.19
country_event = {
	id = ynn_events.7
	title = ynn_events.7.t
	desc = {
		trigger = { has_country_flag = ynn_civil_war_iosahar }	#Triggered by discontinuing the Iosahar system
	   desc = ynn_events.7.d1
	}
	desc = {
	   trigger = { has_country_flag = ynn_civil_war_religion }	#Triggered by converting away from Ynn River Worship
	   desc = ynn_events.7.d2
	}
	picture = ACCUSATION_eventPicture
	
	is_triggered_only = yes
	trigger = {
		NOT = { is_companion = yes }
		liberty_desire = 30
		is_subject_of_type = ynnic_iosahar
	}
	
	immediate = {
		hidden_effect = {
			save_event_target_as = ynn_civil_war_leader
		}
	}
	
	option = {
		name = ynn_events.7.a		
		
		ai_chance = {
			factor = 1
		}
		
		set_country_flag = ynn_iosahar_is_revolting
		grant_independence = yes
		
		FROM = {
			every_subject_country = {
				limit = {
					is_subject_of_type = ynnic_iosahar
					liberty_desire = 30
				}
				set_country_flag = ynn_iosahar_is_revolting
				event_target:ynn_civil_war_leader = {
					create_subject = {
						subject_type = ynnic_iosahar
						subject = PREV
					}
				}
			}
			country_event = { id = ynn_events.8 }
		}
		declare_war_with_cb = {
			who = FROM
			casus_belli = cb_iosahar_annex
		}
	}

	option = {
		name = ynn_events.7.b
		
		ai_chance = {
			factor = 0
		}
		add_prestige = -10
	}
	
	after = {
		event_target:ynn_civil_war_leader = {
			add_country_modifier = {	#Five years of loyal vassals, ensures they contribute to the revolt
				name = ynn_leading_iosahar_revolt
				duration = 1825
			}
		}
	}
}

#The Great Iosahar Insurrection
country_event = {
	id = ynn_events.8
	title = ynn_events.8.t
	desc = {
		trigger = { FROM = { has_country_flag = ynn_civil_war_iosahar } }
	   desc = ynn_events.8.d1
	}
	desc = {
	   trigger = { FROM = { has_country_flag = ynn_civil_war_religion } }
	   desc = ynn_events.8.d2
	}
	picture = CIVIL_WAR_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = ynn_events.8.a		
		
		ai_chance = {
			factor = 1
		}
		
		add_stability = -1
		custom_tooltip = ynn_events_8_tooltip
		#tooltip = {	#broken, tooltip doesn't show
		#	FROM = {
		#		declare_war_with_cb = {
		#			who = ROOT
		#			casus_belli = cb_iosahar_annex
		#		}
		#	}
		#}
	}
	
	after = {
		FROM = {
			clr_country_flag = ynn_civil_war_iosahar
			clr_country_flag = ynn_civil_war_religion
		}
	}
}

country_event = {	#Event used to turn someone into a Iosahar. Use ynn_events.20 if you want it to be hidden
	id = ynn_events.9
	title = ynn_events.9.t
	desc = {
		trigger = { NOT = { has_reform = malacnar_monarchy } }
	   desc = ynn_events.9.d1
	}
	desc = {
	   trigger = { has_reform = malacnar_monarchy }
	   desc = ynn_events.9.d2
	}
	picture = CONQUEST_eventPicture

	is_triggered_only = yes
	
	trigger = {
		NOT = { has_country_flag = ynnic_debug_subjugation }
		#FROM = { has_country_modifier = ynn_diplo }
	}
	
	immediate = {
		set_country_flag = ynnic_debug_subjugation
		set_country_flag = ynnic_debug_subjugation_2
	}
	
	option = {
		name = ynn_events.9.a
		ai_chance = { factor = 1 }
		hidden_effect = { clr_country_flag = ynnic_debug_subjugation }
		add_prestige = -25
		every_subject_country = {
			limit = {
				is_subject_of_type = ynnic_iosahar
			}
			FROM = {
				create_subject = {
					subject_type = ynnic_iosahar
					subject = PREV
				}
			}
		}
		tooltip = {
			FROM = {
				create_subject = {
					subject_type = ynnic_iosahar
					subject = ROOT
				}
			}
		}
		hidden_effect = {
			if = {	#cleaning allies for an overlord
				limit = {
					FROM = {
						ynnic_iosahar = 1
						has_country_modifier = ynn_diplo
						num_of_allies = 1
					}
				}
				ROOT = {
					if = {
						limit = {
							is_subject_of = FROM
							is_subject_of_type = ynnic_iosahar
						}
					}
					create_subject = {
						subject_type = vassal
						subject = FROM
					}
					FROM = {
						grant_independence = yes
					#	create_subject = {
					#		subject_type = ynnic_iosahar
					#		subject = ROOT
					#	}
					}
				}
			}
		}
		
		if = {
			limit = {
				has_reform = malacnar_monarchy
			}
			FROM = { country_event = { id = flavor_malacnar.200 } }
		}
	}
	
	after = {
		clr_country_flag = ynnic_debug_subjugation_2
		FROM = {	#Aftereffect takes care of the double overlords bug
			create_subject = {
				subject_type = ynnic_iosahar
				subject = ROOT
			}
		}
	}
}

#Iosahar Defects Due to Alliance
country_event = {
	id = ynn_events.10
	title = ynn_events.10.t
	desc = ynn_events.10.d
	picture = SIEGE_eventPicture

	is_triggered_only = yes
	
	trigger = {
		FROM = {
			has_country_modifier = ynn_diplo
			NOT = { has_country_flag = ynnic_debug_subjugation_2 }	#To fix a subjugated overlord getting this event on its former iosahar
		}
		any_ally = {
			war_with = FROM
			NOT = {
				is_in_war = {
					attacker_leader = THIS
					defender_leader = FROM
					casus_belli = cb_independence_war
				}
			}
		}
	}
	
	option = {
		name = ynn_events.10.a		
		
		ai_chance = {
			factor = 1
		}
		
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = ynn_overlord_at_war_with_iosahar
			}
			add_trust = {
				who = ROOT
				value = -5
				mutual = yes	#Mutual loss of trust is so that you won't purposefully declare war on your Iosahar's allies to use the Iosahar as a piggy bank
			}
			add_prestige = -5 
		}
		
		custom_tooltip = you_as_iosahar_defect_tooltip
		hidden_effect = { FROM = { country_event = { id = ynn_events.11 } } }
	}
}

#Overlord is Informed of Iosahar Defection
country_event = {
	id = ynn_events.11
	title = ynn_events.11.t
	desc = ynn_events.11.d
	picture = SIEGE_eventPicture

	is_triggered_only = yes
	
	#hidden = yes #Can hide it if it's too obnoxious
	
	option = {
		name = ynn_events.11.a		
		
		ai_chance = {
			factor = 1
		}
		
		tooltip = {
			add_opinion = {
				who = FROM
				modifier = ynn_overlord_at_war_with_iosahar
			}
			add_trust = {
				who = FROM
				value = -5
				mutual = yes
			}
			add_prestige = -5 
		}
	
		custom_tooltip = iosahar_defects_tooltip
	}
}

#Iosahar-overlord war ends
country_event = {
	id = ynn_events.12
	title = ynn_events.12.t
	desc = ynn_events.12.d
	picture = REFORM_eventPicture

	is_triggered_only = yes
	
	trigger = {
		FROM = { has_country_modifier = ynn_diplo }	#Defeated by a country that observes Ynnic Diplomacy
	}
	
	hidden = yes
	
	option = {
		name = ynn_events.12.a		
		
		ai_chance = {
			factor = 1
		}
		
		country_event = { id = ynn_events.13 }
		every_ally = { country_event = { id = ynn_events.13 } }
	}
}

#Sorter Event
country_event = {
	id = ynn_events.13
	title = ynn_events.13.t
	desc = ynn_events.13.d
	picture = RELIGION_eventPicture

	is_triggered_only = yes
	
	hidden = yes
	
	trigger = {
		any_known_country = {
			OR = {
				has_opinion_modifier = {	
					who = ROOT
					modifier = ynn_overlord_at_war_with_iosahar
				}
				reverse_has_opinion_modifier = {	
					who = ROOT
					modifier = ynn_overlord_at_war_with_iosahar
				}
			}
		}
	}
	
	option = {
		name = ynn_events.13.a
		
		ai_chance = {
			factor = 1
		}
		
		random_known_country = {	#Checks if you are a Iosahar defector, you regain them
			limit = { 
				has_opinion_modifier = {	
					who = ROOT
					modifier = ynn_overlord_at_war_with_iosahar
				}
			}
			country_event = { id = ynn_events.14 }
			overlord = {			#In case the overlord has been made into a Iosahar, the new overlord gets it. This scenario is currently impossible, as making an overlord into a Iosahar will make them lose the defector
				country_event = { id = ynn_events.14 } 
				set_country_flag = ynn_14_overlord
			}
		}
		
		random_known_country = {	#Checks if you have any Iosahar defector, you lose em
			limit = { 
				reverse_has_opinion_modifier = {	
					who = ROOT
					modifier = ynn_overlord_at_war_with_iosahar
				}
			}
			country_event = { id = ynn_events.15 }
		}
	}
}

#Your Iosahar is returned
country_event = {
	id = ynn_events.14
	title = ynn_events.14.t
	desc = {
		trigger = { NOT = { has_country_flag = ynn_14_overlord } }
	   desc = ynn_events.14.d1
	}
	desc = {
	   trigger = { has_country_flag = ynn_14_overlord }
	   desc = ynn_events.14.d2
	}
	picture = CONQUEST_eventPicture

	is_triggered_only = yes
	
	trigger = { is_subject = no }
	
	immediate = {
		hidden_effect = {
			remove_opinion = {
				who = FROM
				modifier = ynn_overlord_at_war_with_iosahar
			}
			FROM = { country_event = { id = ynn_events.20 } }
		}
	}
	
	option = {
		name = ynn_events.14.a
		
		ai_chance = {
			factor = 1
		}
		
		tooltip = {
			remove_opinion = {
				who = FROM
				modifier = ynn_overlord_at_war_with_iosahar
			}
			create_subject = {
				subject_type = ynnic_iosahar
				subject = FROM
			}
		}
		
		add_prestige = 10
		
		clr_country_flag = ynn_14_overlord
	}
}

#Defector Iosahar goes free
country_event = {
	id = ynn_events.15
	title = ynn_events.15.t
	desc = ynn_events.15.d
	picture = RELIGION_eventPicture

	is_triggered_only = yes
	
	hidden = yes
	
	immediate = {
		hidden_effect = {
			FROM = {
				remove_opinion = {
					who = ROOT
					modifier = ynn_overlord_at_war_with_iosahar
				}
			}
		}
	}
	
	option = {
		name = ynn_events.15.a
		
		ai_chance = {
			factor = 1
		}
		
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = vassal_broke_free
			}
			add_trust = {
				who = ROOT
				value = -5
				mutual = yes
			}
			add_prestige = -5
			country_event = { id = ynn_events.16 }
		}
	}
}

#You are informed your defector Iosahar went free
country_event = {
	id = ynn_events.16
	title = ynn_events.16.t
	desc = ynn_events.16.d
	picture = COUNTRY_COLLAPSE_eventPicture

	is_triggered_only = yes
	
	option = {
		name = ynn_events.16.a
		
		ai_chance = {
			factor = 1
		}
		
		tooltip = {
			add_opinion = {
				who = FROM
				modifier = vassal_broke_free
			}
			add_trust = {
				who = FROM
				value = -5
				mutual = yes
			}
			add_prestige = -5
		}
	}
}

#The Dragon Kingdom - Rzentur ditch Ynnic Diplomacy
country_event = {
	id = ynn_events.17
	title = ynn_events.17.t
	desc = ynn_events.17.d
	picture = CONQUEST_eventPicture

	is_triggered_only = yes
	#fire_only_once = yes
	major = yes
	
	trigger = {	#Is a Drozma Tur Rzentur who has vassalized or annexed every other Rzentur.
		religion = drozma_tur
		has_country_modifier = ynn_diplo
		is_subject = no
		NOT = { is_year = 1500 }	#Stops the checks after the first century, just end the iosahar by decision afterwards
		NOT = { has_country_modifier = ynn_leading_iosahar_revolt }	#Pomvasonn led a iosahar rebellion just to turn everybody into normal vassals, in one test game
		primary_culture = rzentur
		all_known_country = {
			OR = {
				NOT = { religion = drozma_tur }
				is_subject_of = ROOT
			}
		}
	}
	
	immediate = {	#If you have non-Drozma Tur / non-Rzentur subjects, they rise up in rebellion
		hidden_effect = {
			random_subject_country = {
				limit = {
					is_subject_of_type = ynnic_iosahar
					NOT = { primary_culture = rzentur }
					liberty_desire = 20
					all_known_country = {
						NOT = {
							AND = {
								is_subject_of_type = ynnic_iosahar
								is_subject_of = ROOT
								total_development = PREV
								NOT = { primary_culture = rzentur }
								liberty_desire = 20
							}
						}
					}
				}
				set_country_flag = ynn_civil_war_iosahar
				save_event_target_as = iosahar_ynn_17
			}
		}
	}
	
	option = {
		name = ynn_events.17.a
		
		ai_chance = {
			factor = 1
		}
		
		remove_country_modifier = ynn_diplo
		
		if = {
			limit = {
				ynnic_iosahar = 1
			}
			custom_tooltip = ynn_17_tooltip
			hidden_effect = {
				every_subject_country = {
					limit = {
						is_subject_of_type = ynnic_iosahar
						primary_culture = rzentur
						NOT = { has_country_flag = ynn_civil_war_iosahar }
					}
					remove_country_modifier = ynn_diplo
					ROOT = {
						create_subject = {
							subject_type = vassal
							subject = PREV
						}
					}
				}
				every_subject_country = {
					add_liberty_desire = 20
					add_trust = {
						who = ROOT
						value = -10
						mutual = no
					}
				}
			}
			
			if = {	#Checks if there's enough non-Rzentur / non-DT countries for a rebellion
				limit = {
					any_subject_country = {
						is_subject_of_type = ynnic_iosahar
						has_country_flag = ynn_civil_war_iosahar
					}
					any_subject_country = {
						is_subject_of_type = ynnic_iosahar
						NOT = { primary_culture = rzentur }
						NOT = { has_country_flag = ynn_civil_war_iosahar }
						liberty_desire = 20
					}
				}
				custom_tooltip = ynn_17_tooltip_really_good_idea
				hidden_effect = {
					event_target:iosahar_ynn_17 = {
						set_country_flag = ynn_civil_war_iosahar
						country_event = { id = ynn_events.7 }
					}
				}
			}
			else = {
				hidden_effect = {
					event_target:iosahar_ynn_17 = {
						remove_country_modifier = ynn_diplo
						ROOT = {
							create_subject = {
								subject_type = vassal
								subject = PREV
							}
						}
						clr_country_flag = ynn_civil_war_iosahar
					}
					every_subject_country = {
						limit = {
							is_subject_of_type = ynnic_iosahar
							NOT = { primary_culture = rzentur }
						}
						remove_country_modifier = ynn_diplo
						ROOT = {
							create_subject = {
								subject_type = vassal
								subject = PREV
							}
						}
					}
				}
			}
		}
		
		add_country_modifier = {
			name = ruling_like_dragons
			duration = 7300
		}
	}
	
	option = {
		name = ynn_events.17.b
		
		trigger = {
			ynnic_iosahar = 2
		}
		
		ai_chance = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { tag = G23 }
			}
		}
	}
}

#iosahar drifts away
country_event = {
	id = ynn_events.18
	title = ynn_events.18.t
	desc = {
		trigger = { is_subject_of_type = tributary_state }
		desc = ynn_events.18.d1
	}
	desc = {
		trigger = { is_subject = no }
		desc = ynn_events.18.d2
	}
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = ynn_events.18.a
		ai_chance = {
			factor = 1
		}
		
		if = {
			limit = { is_subject_of_type = tributary_state }
			tooltip = {
				FROM = {
					create_subject = {
						subject_type = tributary_state
						subject = ROOT
					}
				}
			}
		}
		
		FROM = {
			add_trust = {
				who = ROOT
				value = -10
				mutual = yes
			}
		}
	}
}

#iosahar doesn't get inherited
country_event = {
	id = ynn_events.19
	title = ynn_events.19.t
	desc = ynn_events.19.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	hidden = yes
	
	trigger = {
		is_subject_of_type = ynnic_iosahar
		FROM = {
			NOT = { has_country_modifier = ynn_diplo }
			NOT = {
				has_opinion_modifier = {
					who = ROOT
					modifier = ynn_revolting_iosahar
				}
			}
		}
		is_companion = no
	}
	
	option = {
		name = ynn_events.19.a
		ai_chance = {
			factor = 1
		}
		
		grant_independence = yes
		
	}
}

country_event = {	#Hidden event used to turn someone into a Iosahar. Use ynn_events.9 if you want it to be visible
	id = ynn_events.20
	title = ynn_events.20.t
	desc = ynn_events.20.d
	picture = RELIGION_eventPicture

	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		NOT = { has_country_flag = ynnic_debug_subjugation }
		#FROM = { has_country_modifier = ynn_diplo }
	}
	
	immediate = {
		set_country_flag = ynnic_debug_subjugation
		set_country_flag = ynnic_debug_subjugation_2
	}
	
	option = {
		name = ynn_events.20.a
		ai_chance = { factor = 1 }
		hidden_effect = { clr_country_flag = ynnic_debug_subjugation }
		every_subject_country = {
			limit = {
				is_subject_of_type = ynnic_iosahar
			}
			FROM = {
				create_subject = {
					subject_type = ynnic_iosahar
					subject = PREV
				}
			}
		}
		tooltip = {
			FROM = {
				create_subject = {
					subject_type = ynnic_iosahar
					subject = ROOT
				}
			}
		}
		hidden_effect = {
			if = {	#cleaning allies for an overlord
				limit = {
					FROM = {
						ynnic_iosahar = 1
						has_country_modifier = ynn_diplo
						num_of_allies = 1
					}
				}
				ROOT = {
					if = {
						limit = {
							is_subject_of = FROM
							is_subject_of_type = ynnic_iosahar
						}
					}
					create_subject = {
						subject_type = vassal
						subject = FROM
					}
					FROM = {
						grant_independence = yes
					#	create_subject = {
					#		subject_type = ynnic_iosahar
					#		subject = ROOT
					#	}
					}
				}
			}
		}
	}
	
	after = {
		clr_country_flag = ynnic_debug_subjugation_2
		FROM = {	#Aftereffect takes care of the double overlords bug
			create_subject = {
				subject_type = ynnic_iosahar
				subject = ROOT
			}
		}
	}
}

country_event = {	#Background event to transfer Iosahar of Iosahar to the top overlord
	id = ynn_events.21
	title = ynn_events.21.t
	desc = ynn_events.21.d
	picture = RELIGION_eventPicture

	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		ynnic_iosahar = 1
		is_subject = no
		any_subject_country = {
			is_subject_of_type = ynnic_iosahar
			ynnic_iosahar = 1
		}
	}
	
	option = {
		name = ynn_events.21.a
		ai_chance = { factor = 1 }
		
		every_subject_country = {
			limit = {
				is_subject_of_type = ynnic_iosahar
				ynnic_iosahar = 1
			}
			every_subject_country = {
				limit = {
					is_subject_of_type = ynnic_iosahar
				}
				grant_independence = yes # So you don't get a double subject
				country_event = { id = ynn_events.9 }
			}
		}
	}
}

#Visitors to Adbrabohvi
country_event = {
	id = ynn_events.22
	title = ynn_events.22.t
	desc = ynn_events.22.d
	picture = TRADE_GOODS_FURS_FISH_AND_SALT_eventPicture
	
	fire_only_once = yes
	major = yes
	
	trigger = {
		owns = 1133
		any_country = {
			has_discovered = 1133
			NOT = {
				technology_group = tech_ynnic
				culture_is_ruinborn = yes
			}
		}
	}
	
	option = {
		name = ynn_events.22.a

		medium_increase_of_human_tolerance_effect = yes
		medium_increase_of_halfling_tolerance_effect = yes
		medium_increase_of_elven_tolerance_effect = yes
		medium_increase_of_dwarven_tolerance_effect = yes
		medium_increase_of_half_orcish_tolerance_effect = yes
		
		1133 = { 
			center_of_trade = 1
		}
		add_country_modifier = {
			name = adbrabohvi_cultural_shock
			duration = 7300
		}
		
		if = {
			limit = {
				has_country_flag = yrw_1a	#Debating for inclusiveness
			}
			every_subject_country = {
				limit = { is_subject_of_type = ynnic_iosahar }
				yrw_iosahar_liberty_ally_1 = yes
			}
		}
		else_if = {
			limit = {
				has_country_flag = yrw_1b	#Debating for excluding non Ynnics
			}
			every_subject_country = {
				limit = { is_subject_of_type = ynnic_iosahar }
				yrw_iosahar_liberty_opposition_1 = yes
			}
		}
		else_if = {
			limit = {	#Contradicting your chosen reform
				OR = {
					has_country_flag = yrw_1b_reform
					has_country_modifier = yrw_1b
					overlord = { has_country_flag = yrw_1b_reform }
					overlord = { has_country_modifier = yrw_1b }
				}
			}
			reduce_stability_or_adm_power = yes
		}
		
		ai_chance = {
			factor = 1
		}
	}
		
	option = {
		name = ynn_events.22.b

		medium_decrease_of_human_tolerance_effect = yes
		medium_decrease_of_halfling_tolerance_effect = yes
		medium_decrease_of_elven_tolerance_effect = yes
		medium_decrease_of_dwarven_tolerance_effect = yes
		medium_decrease_of_half_orcish_tolerance_effect = yes
		
		if = {
			limit = {
				has_country_flag = yrw_1a	#Debating for inclusiveness
			}
			every_subject_country = {
				limit = { is_subject_of_type = ynnic_iosahar }
				yrw_iosahar_liberty_opposition_1 = yes
			}
		}
		else_if = {
			limit = {
				has_country_flag = yrw_1b	#Debating for excluding non Ynnics
			}
			add_stability_or_adm_power = yes
			every_subject_country = {
				limit = { is_subject_of_type = ynnic_iosahar }
				yrw_iosahar_liberty_ally_1 = yes
			}
		}
		else_if = {
			limit = {	#Stabilizing so long as you're not contradicting your chosen reform
				NOT = {
					has_country_flag = yrw_1a
					has_country_flag = yrw_1a_reform
					has_country_modifier = yrw_1a
					overlord = { has_country_flag = yrw_1a_reform }
					overlord = { has_country_modifier = yrw_1a }
				}
			}
			add_stability_or_adm_power = yes
		}
		
		random = {
			chance = 50
			custom_tooltip = ynn_22_tooltip
		}
		random = {
			chance = 50
			1133 = {
				spawn_rebels = {
					type = pretender_rebels
					size = 2
					culture = vernman
					religion = regent_court	#might be too early for Corinite
					leader = "Aril 'the Courteous'"
					leader_dynasty = "sina Redcoast"
					win = no
				}
			}
		}
		
		ai_chance = {	#Non-canon option, would be annoying for players to lose out on a CoT
			factor = 0
		}
	}
}

province_event = {	#Pomentere Estates
	id = ynn_events.23
	title = ynn_events.23.t
	desc = ynn_events.23.d
	picture = NATIVES_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		owner = { has_country_flag = has_pomentere_estates }
	}
	
	#Update the modifier
	option = {
        name = ynn_events.23.a
		if = {
			limit = {
				culture = veykodan
				has_owner_accepted_culture = yes
			}
			add_province_modifier = {
				name = ynn_pomentere_estates
				duration = -1
			}
		}
		else = {
			remove_province_modifier = ynn_pomentere_estates
		}
	}
}

country_event = {	#The Dragon Cult's Last Stand
	id = ynn_events.24
	title = ynn_events.24.t
	desc = ynn_events.24.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = drozma_fighting_last_stand
	}
	
	immediate = {
		hidden_effect = {
			random_province = {
				limit = {
					province_id = 1191
				}
				save_event_target_as = drozma_last_stand_prov
			}
			if = {
				limit = {
					is_year = 1480
				}
				set_country_flag = drozma_grown_prince
			}
			#random_province = {
			#	limit = {
			#		has_province_modifier = drozma_dragon_prince
			#	}
			#	save_event_target_as = drozma_last_stand_prov
			#}
		}
	}
	
	option = {
        name = ynn_events.24.a		#Dolenmach + DT fanatics + Dragon itself fight against you
		if = {
			limit = { has_country_flag = drozma_grown_prince }
			event_target:drozma_last_stand_prov = {
				spawn_rebels = {
					type = drozma_tur_rebels
					size = 3
					leader = "Erethra Dolenmach"
				}
				set_province_flag = drozma_last_stand_prov_flag
			}
		}
		else = {
			event_target:drozma_last_stand_prov = {
				spawn_rebels = {
					type = drozma_tur_rebels
					size = 2
					leader = "Erethra Dolenmach"
				}
				set_province_flag = drozma_last_stand_prov_flag
			}
		}
	}
}

country_event = {	#Controller event for 26
	id = ynn_events.25
	title = ynn_events.25.t
	desc = ynn_events.25.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		always = yes
	}
	
	option = {
        name = ynn_events.25.a
		
		if = {
			limit = {
				has_country_flag = drozma_fighting_last_stand
			}
			country_event = { id = ynn_events.26 }
		}
		else = {
			random_known_country = {
				limit = {
					has_country_flag = drozma_fighting_last_stand
				}
				country_event = { id = ynn_events.26 }
			}
		}
	}
}

country_event = {	#You beat the Dragon Cult's last stand
	id = ynn_events.26
	title = ynn_events.26.t
	desc = {
		trigger = { NOT = { has_country_flag = drozma_grown_prince } }
	   desc = ynn_events.26.d1
	}
	desc = {
	   trigger = { 
			has_country_flag = drozma_grown_prince
			NOT = { has_country_flag = drozma_leader_v_dragon }
		}
	   desc = ynn_events.26.d2
	}
	desc = {
	   trigger = { 
			has_country_flag = drozma_grown_prince 
			has_country_flag = drozma_leader_v_dragon
			has_country_flag = drozma_die_to_dragon
	   }
	   desc = ynn_events.26.d3	#Leader fights dragon and dies
	}
	desc = {
	   trigger = { 
			has_country_flag = drozma_grown_prince 
			has_country_flag = drozma_leader_v_dragon
			has_country_flag = drozma_slay_dragon
	   }
	   desc = ynn_events.26.d4	#Leader fights dragon and wins
	}
	picture = IMPALED_SOLDIERS_eventPicture
	
	is_triggered_only = yes
	major = yes
	
	trigger = {
		has_country_flag = drozma_fighting_last_stand
	}
	
	immediate = {
		hidden_effect = {
			if = {
				limit = {
					has_country_flag = drozma_grown_prince
					has_country_flag = drozma_leader_v_dragon
					calc_true_if = {
						mil = 6
						mil = 5
						ruler_has_mage_personality = yes
						ruler_has_personality = tactical_genius_personality
						ruler_has_personality = bold_fighter_personality
						ruler_has_personality = strict_fighter_personality
						ruler_has_personality = inspiring_leader_personality
						ruler_has_personality = martial_educator_personality
						has_ruler_modifier = g32_legendary_battleking
						has_ruler_modifier = g32_veteran_battleking
						tag = G34	 #Arganjuzorn
						amount = 3
					}
				}
				set_country_flag = drozma_slay_dragon
			}
			else_if = {
				limit = {
					has_country_flag = drozma_grown_prince
					has_country_flag = drozma_leader_v_dragon
				}
				set_country_flag = drozma_die_to_dragon
			}
			random_province = {
				limit = {
					has_province_flag = drozma_last_stand_prov_flag
				}
				save_event_target_as = drozma_last_stand_prov
				clr_province_flag = drozma_last_stand_prov_flag
			}
			if = {
				limit = { has_country_flag = drozma_grown_prince }
				event_target:drozma_last_stand_prov = {
					kill_units = {
						who = ROOT
						type = infantry
						amount = 5
					}
				}
			}
		}
	}
	
	option = {
        name = ynn_events.26.a
		
		if = {
			limit = {
				has_country_flag = drozma_slay_dragon
			}
			add_stability_or_adm_power = yes
			add_prestige = 50
			add_splendor = 200
			malacnar_battleking_rankup = yes
			add_authority = 20
			add_ruler_modifier = {
				name = ynn_dragonslayer
				duration = -1
			}
		}
		else_if = {
			limit = {
				has_country_flag = drozma_die_to_dragon
			}
			hidden_effect = {		#Monarch barge flows down from Rzenta instead of your capital as usual
				set_country_flag = buried_at_drozma_last_stand	
				event_target:drozma_last_stand_prov = { set_province_flag = buried_at_drozma_last_stand_prov }
			}
			kill_ruler = yes
			add_prestige = 40
			add_splendor = 150
			add_authority = 20
		}
		else = {
			add_prestige = 30
			add_splendor = 100
			add_authority = 10
		}
		
		if = {
			limit = {
				has_country_flag = drozma_grown_prince
			}
			tooltip = {
				event_target:drozma_last_stand_prov = {
					kill_units = {
						who = ROOT
						type = infantry
						amount = 5
					}
				}
			}
		}
		
		custom_tooltip = ynn_26_tooltip
		
		hidden_effect = {
			every_known_country = {
				limit = {
					OR = {
						religion = ynn_river_worship
						religion = ynn_river_reformed
					}
				}
				add_opinion = {
					who = ROOT
					modifier = yrw_dragonslayer
				}
			}
			
			every_known_country = {
				limit = {
					religion = drozma_tur
				}
				country_event = { id = ynn_events.29 } #Dragonless Once More
			}
			
			ynn_superregion = {
				limit = { religion = drozma_tur }
				
				change_religion = ROOT
				if = {
					limit = { owned_by = ROOT }
					heretic_rebels = 1
				}
			}
			clr_country_flag = drozma_fighting_last_stand
			clr_country_flag = drozma_leader_v_dragon
			clr_country_flag = drozma_die_to_dragon
			clr_country_flag = drozma_slay_dragon
		}
	}
}

country_event = {	#Controller event for 28
	id = ynn_events.27
	title = ynn_events.27.t
	desc = ynn_events.27.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		always = yes
	}
	
	option = {
        name = ynn_events.27.a
		
		if = {
			limit = {
				has_country_flag = drozma_fighting_last_stand
			}
			country_event = { id = ynn_events.28 }
		}
		else = {
			random_known_country = {
				limit = {
					has_country_flag = drozma_fighting_last_stand
				}
				country_event = { id = ynn_events.28 }
			}
		}
	}
}

country_event = {	#Dragon Cult's last stand beats you
	id = ynn_events.28
	title = ynn_events.28.t
	desc = ynn_events.28.d
	picture = REVOLUTION_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = drozma_fighting_last_stand
	}
	
	immediate = {
		hidden_effect = {
			clr_country_flag = drozma_fighting_last_stand
			random_province = {
				limit = {
					has_province_flag = drozma_last_stand_prov_flag
				}
				save_event_target_as = drozma_last_stand_prov
				clr_province_flag = drozma_last_stand_prov_flag
			}
		}
	}
	
	option = {
        name = ynn_events.28.a
		
		event_target:drozma_last_stand_prov = {
			if = {
				limit = {
					owned_by = ROOT
				}
				if = {
					limit = {
						any_core_country = {
							NOT = { tag = ROOT }
							exists = yes
						}
					}
					random_core_country = {
						limit = {
							NOT = { tag = ROOT }
							exists = yes
						}
						event_target:drozma_last_stand_prov = {
							cede_province = PREV
						}
						if = {
							limit = {
								is_subject = yes
							}
							grant_independence = yes
						}
						add_truce_with = ROOT
					}
				}
				else = {
					random_core_country = {
						limit = {
							NOT = { tag = ROOT }
						}
						ROOT = {
							release = PREV
						}
						hidden_effect = {
							change_religion = drozma_tur
						}
						add_truce_with = ROOT
					}
				}
			}
			else_if = {
				limit = {
					owner = { is_subject = yes }
				}
				owner = {
					grant_independence = yes
					add_truce_with = ROOT
				}
			}
			kill_units = {
				who = REB
			}
		}
	}
	
	after = {
		hidden_effect = {
			event_target:drozma_last_stand_prov = {
				owner = {
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					infantry = PREV
					cavalry = PREV
				}
			}
		}
	}
}

country_event = {	#Dragonless Once More
	id = ynn_events.29
	title = ynn_events.29.t
	desc = ynn_events.29.d
	picture = IMPALED_SOLDIERS_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		religion = drozma_tur
	}
	
	option = {
        name = ynn_events.29.a
		
		add_prestige = -20
		add_stability = -3
		
		add_country_modifier = {
			name = begrudging_conversion
			duration = 3650
		}
		
		add_opinion = {
			who = FROM
			modifier = dt_dragonslayer
		}
		add_trust = {
			who = FROM
			value = -50
			mutual = no
		}
		change_religion = FROM
		set_ruler_religion = FROM
		set_consort_religion = FROM
		set_heir_religion = FROM
		
		every_owned_province = {
			limit = { religion = drozma_tur }
			change_religion = FROM
		}
		
		hidden_effect = {
			if = {
				limit = {
					NOT = { 
						is_subject_of = FROM 
						is_companion = yes
					}
				}
				grant_independence = yes
				if = {
					limit = {
						FROM = { has_country_modifier = ynn_diplo }
					}
					FROM = {
						create_subject = {
							subject_type = ynnic_iosahar
							subject = ROOT
						}
					}
				}
				else = {
					FROM = {
						create_subject = {
							subject_type = vassal
							subject = ROOT
						}
					}
				}
			}
		}
	}
}

#Iosahar Revolts
country_event = {
	id = ynn_events.30
	title = ynn_events.30.t
	desc = ynn_events.30.d
	picture = SIEGE_eventPicture

	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		OR = {
			is_in_war = {
				attackers = ROOT
				defender_leader = FROM
				casus_belli = cb_independence_war
			}
			is_in_war = {
				attacker_leader = ROOT
				defender_leader = FROM
				casus_belli = cb_iosahar_annex
			}
			any_ally = {	#Allies of rebel who are about to join don't count as being at war in this exact moment
				war_with = FROM
				is_in_war = {
					attacker_leader = THIS
					defender_leader = FROM
					casus_belli = cb_independence_war
				}
			}
			has_country_flag = ynn_iosahar_is_revolting
		}
	}
	
	option = {
		name = ynn_events.30.a		
		
		ai_chance = {
			factor = 1
		}
		
		FROM = {
			add_opinion = {
				who = ROOT
				modifier = ynn_revolting_iosahar
			}
			add_trust = {
				who = ROOT
				value = -5
				mutual = yes	#Mutual loss of trust is so that you won't purposefully declare war on your Iosahar's allies to use the Iosahar as a piggy bank
			}
			add_prestige = -5 
		}
		clr_country_flag = ynn_iosahar_is_revolting
	}
}

# country_event = { #Dam Maintenance
	# id = ynn_events.23
	# title = ynn_events.23.t
	# desc = ynn_events.23.d
	# picture = RELIGION_eventPicture

	# is_triggered_only = yes
	
	# trigger = {
		# has_country_flag = ynn_recently_maintained_dam_flag
	# }
	
	# option = { 
		# name = ynn_events.23.a
		# ai_chance = { factor = 1 }
		# clr_country_flag = ynn_recently_maintained_dam_flag
	# }
# }

country_event = { #Dam Maintenance
	id = ynn_events.50
	title = ynn_events.50.t
	desc = ynn_events.50.d
	picture = RELIGION_eventPicture

	is_triggered_only = yes
	
	trigger = {
		 OR = {
			1186 = { #Mocbarja
				owned_by = ROOT
			}
			1165 = { #Bosancovac
				owned_by = ROOT
			}
			1138 = { #Vels Bacar
				owned_by = ROOT
			}
			1133 = { #Adbrabohvi
				owned_by = ROOT
			}
		}
		capital_scope = {
			superregion = ynn_superregion
		}
		NOT = {
			has_country_flag = ynn_recently_maintained_dam_flag
		}
	}
	immediate = {
		 hidden_effect = {
			 random_list = {
				1 = {
					trigger = {
						1186 = { #Mocbarja
							owned_by = ROOT
						}
					}
					1186 = {
						save_event_target_as = ynnic_dam_maintenance_event_target
					}
				}
				1 = {
					trigger = {
						1165 = { #Bosancovac
							owned_by = ROOT
						}
					}
					1165 = {
						save_event_target_as = ynnic_dam_maintenance_event_target
					}
				}
				1 = {
					trigger = {
						1138 = { #Vels Bacar
							owned_by = ROOT
						}
					}
					1138 = {
						save_event_target_as = ynnic_dam_maintenance_event_target
					}
				}
				1 = {
					trigger = {
						1133 = { #Adbrabohvi
							owned_by = ROOT
						}
					}
					1133 = {
						save_event_target_as = ynnic_dam_maintenance_event_target
					}
				}
			}
		}
	}
	option = { #Let it Decay, we can't afford it.
		name = ynn_events.50.a
		ai_chance = { factor = 1 }
		event_target:ynnic_dam_maintenance_event_target = {
			
		}
	}
	option = { #
		name = ynn_events.50.b
		ai_chance = { factor = 1 }
	}
	option = { #Let us hire dwarves to aid in it's construction!
		name = ynn_events.50.c
		ai_chance = { factor = 1 }
		trigger = {
			OR = {
				any_known_country = {
					tag = G94
				}
				 any_owned_province = {
					has_dwarven_minority_trigger = yes
					has_dwarven_majority_trigger = yes
				}
			}
		}
	 }
 }

country_event = { #Prepare infrastructure Tromseloc way
	id = ynn_events.200
	title = ynn_events.200.t
	desc = ynn_events.200.d
	picture = EXPLORERS_eventPicture

	is_triggered_only = yes

	trigger = {
		technology_group = tech_ynnic
	}

	option = {
		name = ynn_events.200.a
		2781 = {
			add_siberian_construction = 400
		}
	}
}